package es.esy.viksu.dialogtraining;

import com.firebase.client.Firebase;

/**
 * Created by viktor.suwiyanto on 19/07/2016.
 */
public class FirebaseApp extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Firebase.setAndroidContext(this);
    }
}
