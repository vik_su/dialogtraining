package es.esy.viksu.dialogtraining;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by viktor.suwiyanto on 19/07/2016.
 */
public class AddItemDialog extends DialogFragment implements TextView.OnEditorActionListener {

    private EditText editTextKey,editTextValue;

    public AddItemDialog() {
    }

    public interface AddItemDialogListener {
        void onFinishEditDialog(String key,String value);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_list, container);
        editTextKey = (EditText) view.findViewById(R.id.txt_itemKey);
        editTextValue = (EditText) view.findViewById(R.id.txt_itemValue);
        getDialog().setTitle("Hello");

        // Show soft keyboard automatically
        editTextKey.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        editTextValue.setOnEditorActionListener(this);

        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
            // Return input text to activity
            AddItemDialogListener activity = (AddItemDialogListener) getActivity();
            activity.onFinishEditDialog(editTextKey.getText().toString(),editTextValue.getText().toString());
            this.dismiss();
            return true;
        }
        return false;
    }

}
